import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/tree',
    name: 'Tree',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "tree" */ '../views/index.vue')
  },
  {
    path: '/user',
    name: 'User',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "user" */ '../views/user/list.vue')
  },
  {
    path: '/role',
    name: 'role',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "role" */ '@/views/role/page.vue')
  },
  {
    path: '/page',
    name: '菜单',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "user" */ '../views/page.vue')
  },
  {
    path: '/level',
    name: 'level',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "member" */ '@/views/member/page.vue')
  },
  {
    path: '/menu',
    name: 'menu',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "menu" */ '@/views/role/menu.vue')
  },
  {
    path: '/member',
    name: '会员管理',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "member" */ '@/views/member/mber.vue')
  },
  {
    path: '/integration',
    name: '积分',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "member" */ '@/views/member/integration.vue')
  },
  {
    path: '/carComment',
    name: '车辆评论',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "member" */ '@/views/member/comment.vue')
  },
  {
    path: '/order',
    name: '订单管理',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "member" */ '@/views/member/order.vue')
  },
  {
    path: '/car',
    name: 'car',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "car" */ '@/views/car/cars.vue')
  },
  {
    path: '/carType',
    name: 'carType',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carType" */ '@/views/car/carType.vue')
  },
  {
    path: '/carConfig',
    name: 'carConfig',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carConfig" */ '@/views/car/carConfig.vue')
  },
  {
    path: '/advertise',
    name: 'advertise',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carType" */ '@/views/adverties/advertise.vue')
  },
  {
    path: '/advertisePosition',
    name: 'advertisePosition',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carType" */ '@/views/adverties/advertisePosition.vue')
  },
  {
    path: '/coupon',
    name: 'coupon',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carType" */ '@/views/adverties/coupon.vue')
  },
  {
    path: '/couponRecord',
    name: 'couponRecord',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carType" */ '@/views/adverties/couponRecord.vue')
  },
  {
    path: '/question',
    name: 'customerIssues',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carType" */ '@/views/adverties/customerIssues.vue')
  },
  {
    path: '/operLog',
    name: '操作日志记录',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carConfig" */ '@/views/user/oper_log.vue')
  },
  {
    path: '/orderLog',
    name: '操作日志记录',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carConfig" */ '@/views/echarts/order-log-show.vue')
  },
  {
    path: '/loginLog',
    name: '登录日志记录',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "carConfig" */ '@/views/user/login_log.vue')
  },
  {path: '/brand',
    name: '汽车品牌',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/car/brand.vue')
  },
  {
    path: '/carShop',
    name: '门店表',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/car/shop.vue')
  },
  {
    path: '/maintainRecord',
    name: '维修记录',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/car/maintainRecord.vue')
  },
  {
    path: '/maintainShop',
    name: '维修厂',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/car/maintainShop.vue')
  },
  {
    path: '/dept',
    name: '部门',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/dept/dept.vue')
  },
  {
    path: '/ecdemo1',
    name: '统计1',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/echarts/demo1.vue')
  },
  {
    path: '/ecdemo2',
    name: '统计2',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/echarts/demo2.vue')
  },
  {
    path: '/ecdemo3',
    name: '统计2',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/echarts/demo3.vue')
  },
  {
    path: '/login-log-show',
    name: '管理员登录统计',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/echarts/login-log-show.vue')
  },
  {
    path: '/oper-log-show',
    name: '网站操作记录统计',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/echarts/oper-log-show.vue')
  },
  {
    path: '/login',
    name: '登录',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/login.vue')
  },
  {
    path: '/admin',
    name: '后台首页',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/defaultTag.vue')
  },
  {
    path: '/dict_data',
    name: '数据字典',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/dict/dict_data.vue')
  },
  {
    path: '/dict_type',
    name: '字典类型',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/dict/dict_type.vue')
  },
  {
    path: '/map',
    name: '地图',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "brand" */ '@/views/menu/map.vue')
  },
  {
    path: '/index',
    name: 'Index',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/index.vue'),
    children:[{
      path: '/',
      name: 'defaultTag',
      redirect:'/defaultTag'
     },{
      path: '/defaultTag',
      name: 'defaultTag',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/views/defaultTag.vue')
    }
    ]
  }, 
]

const router = new VueRouter({
  mode: 'hash',
  routes
})

export default router
