import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios'
import App from './App.vue'
import router from './router'
import store from './store'
import * as echarts from 'echarts'
// collapse 展开折叠
import CollapseTransition from 'element-ui/lib/transitions/collapse-transition';

const axios1 = axios.create({
  timeout: 5000, // 请求超时时间,如果超过5s就会报错
  baseURL: 'http://localhost:11930',// 表示我们请求api时候的基地址,此地址会拼接上请求中的url
  withCredentials:true,
  //headers: { a: 'foobar'} 
  //params:{accessToken:token}
  params:{accessToken:localStorage.getItem("token")}
})

//全局相应拦截，所有的网络请求返回之后都会走这里
axios1.interceptors.response.use(
  function(response) {
    console.log(JSON.stringify(response)+"...444...");
      var token= localStorage.getItem("token");
      //console.log("..00000..."+token+"..............tk1..........");
      if(token==''||token==null){
     // console.log(response.data+'......22222.................')
      window.location.href = '/#/login' // 跳转到登录页
      //this.$router.push("/#/login");
     }
    return response;
  }
)

Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.prototype.$http = axios1;
Vue.prototype.$echarts = echarts;
Vue.component(CollapseTransition.name, CollapseTransition)


new Vue({
  router,
  store,
  // mounted() {
  //   // 默认页面
  //   this.$router.push('/admin')
  // },
  render: h => h(App)
}).$mount('#app')
